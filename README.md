# Simple Ascii Camera

Take your camera, use it as a mirror, and then add some live ascii magic !

![Sample from camera_to_ascii.py"](sample.png "Sample from camera_to_ascii.py")

# Setup & Run

```bash
pip3 install --user -r requirements.txt
./camera_to_ascii.py
```
